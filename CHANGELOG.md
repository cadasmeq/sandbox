# CHANGELOG

## [0.1.0] - 2021-01-24
First release
 
### Added
- new-feature/ branch pass on this release

### Changed

### Fixed
- CHANGELOG: Renamed changelog to changelog.md

